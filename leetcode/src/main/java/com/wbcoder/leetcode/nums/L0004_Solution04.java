package com.wbcoder.leetcode.nums;

import java.util.Deque;
import java.util.LinkedList;

public class L0004_Solution04 {

    /**
      * @description: 有序数组找中位数
      * @author: chengwb
      * @Date: 2020-03-06 23:40
      */
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int aSize = nums1 == null ? 0 : nums1.length;
        int bSize = nums2 == null ? 0 : nums2.length;
        //不交叉的情况
        if (aSize == 0) {
            if (bSize % 2 == 0) {
                return (double) nums2[bSize / 2];
            } else {
                return (double) nums2[bSize / 2 + 1];
            }
        }
        if (bSize == 0) {
            if (aSize % 2 == 0) {
                return (double) nums2[aSize / 2];
            } else {
                return (double) nums2[aSize / 2 + 1];
            }
        }
        //交叉的情况




        return 0.d;
    }

    public static void main(String[] args) {
//        Solution04 solution01 = new Solution04();
//        int[] a1 = {};
//        int[] a2 = {};
//        System.out.println(solution01.findMedianSortedArrays(a1, a2));
//
//
//        Deque deque = new LinkedList();
//
//        deque.peek();

        test();

    }

    public static void test() {
        Integer a = null, b = null;
        if(a != b) System.out.println("aa");

        Object o = null;
        Deque deque = new LinkedList();
        deque.push(o);
        System.out.println(deque.size());
    }

}
