package com.wbcoder.leetcode.nums;

/**
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 * <p>
 * 你可以假设数组中无重复元素。
 */
public class L0035_SearchInsert {

    public int searchInsert(int[] nums, int target) {

        if (nums.length == 0) return 0;
        int left = 0;
        int right = nums.length - 1;
        int mid = 0;
        while (left < right) {
            mid = (left + right) / 2;
            if (nums[mid] < target) {
                left = mid + 1;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else {
                return mid;
            }
        }
        return nums[left] < target ? left + 1 : left;
    }

    public static void main(String[] args) {
        int[] arrays = {1, 3, 5, 6};
        L0035_SearchInsert leetcode_35SearchInsert = new L0035_SearchInsert();
        System.out.println(leetcode_35SearchInsert.searchInsert(arrays, 5));
    }
}
