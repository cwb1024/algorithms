package com.wbcoder.leetcode.nums;

import java.util.HashMap;
import java.util.Map;

/**
 * 这里不是连续字串，只有一个串包含了所有的字母就可以
 * <p>
 * 给你一个字符串 S、一个字符串 T，请在字符串 S 里面找出：包含 T 所有字母的最小子串。
 * <p>
 * 示例：
 * <p>
 * 输入: S = "ADOBECODEBANC", T = "ABC"
 * 输出: "BANC"
 * 说明：
 * <p>
 * 如果 S 中不存这样的子串，则返回空字符串 ""。
 * 如果 S 中存在这样的子串，我们保证它是唯一的答案。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/minimum-window-substring
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class L0076_Solution76 {

    public String minWindow(String s, String t) {

        //利用一个hash表记录hash,记录在遍历的过程中，t字母出现的次数,需要一个
        Map<Character, Integer> hash = new HashMap<Character, Integer>();
        for (char c : t.toCharArray()) {
            int n = hash.get(c) == null ? 0 : hash.get(c);
            hash.put(c, n + 1);
        }


        int count = hash.size();

        String res = "";

        //j 起始位置 ， i 走的位置
        for (int i = 0, j = 0, c = 0; i < s.length(); i++) {

            // 这里做一些边界的判断
            //当前i走的位置，在hash表内找到了这个key ,表明这次是达标了
            if (hash.get(s.charAt(i)) == null) {
                hash.put(s.charAt(i), 0);
            }
            if (hash.get(s.charAt(i)) == 1) {
                c++;
            }

            //hash 表内还需要的个数减去1
            hash.put(s.charAt(i), hash.get(s.charAt(i)) - 1);

            //j的位置是否可以挪动，当t表内的数字小于0，表明有多了，就可以挪动
            while (j<s.length() && hash.get(s.charAt(j)) != null && hash.get(s.charAt(j)) < 0) {
                hash.put(s.charAt(j), hash.get(s.charAt(j)) + 1);
                j++;
            }

            if (c == count) {
                //比较两个指针的位置
                if (res.length() == 0 || res.length() > i + 1 - j) {
                    //如果当前的少的话，就替换结果  Java 截取字符串，[开始索引,结束索引）
                    res = s.substring(j, i + 1);
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        L0076_Solution76 solution76 = new L0076_Solution76();
        System.out.println(solution76.minWindow("ab"
                , "A"));
    }

}
