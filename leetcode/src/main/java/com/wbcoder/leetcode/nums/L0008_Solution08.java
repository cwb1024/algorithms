package com.wbcoder.leetcode.nums;

/**
 * int类型接受值，在范围 -2147483648～2147483647 之间的运算可以正常实现，
 * 如果相加、相乘超过这个范围，就会丢失精度，出错
 * <p>
 * 关于这类的问题那么要怎么解决呢，字符串转整型值，需要取出各个字符的数字，求和计算，然后进行判断计算过后的位数时候超过int类型的最大值，或者最小值
 * <p>
 * 但是如果进行进位操作了，就已经丢失精度了  怎么办
 * <p>
 * <p>
 * 还有对负数的统一处理一般怎么操作的
 */
public class L0008_Solution08 {
    public int myAtoi(String str) {
        if (str == null || str.trim() == null) {
            return 0;
        }
        //需要遍历取合适的值，遍历的过程中，对字符的有效性进行判断
        String s = str.trim();
        int n = s.length();
        //累计有效长度求和， 需要进位操作，保存值
        //数组后面遇到非数字就结束
        //过程中需要记录当前有效值的数值，并且与最大值，最小值进行判断
        //取出第一位的正负数
        int value = 0;
        int neg = 1;
        for (int i = 0; i < n; i++) {
            // 判断正负数
            if (i == 0 && s.charAt(0) == '-') {
                neg = -1;
                continue;
            }
            if (i == 0 && s.charAt(0) == '+') {
                neg = 1;
                continue;
            }

            //判断是否遇到非数字数
            if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                return value * neg;
            }
            //对数字进行累加进位操作
            int last = value;
            char c = s.charAt(i);
            String s1 = String.valueOf(c);
            value *= 10;
            value += Integer.valueOf(s1);
            //进行取余数判断能不能找到原来的值，如果没找到就说明整型异常了
            if (value / 10 != last) {
                return neg == -1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
        }
        return value * neg;
    }

    public static void main(String[] args) {
        L0008_Solution08 solution08 = new L0008_Solution08();

        System.out.println(solution08.myAtoi(" -ssss1010023630o4"));
    }
}
