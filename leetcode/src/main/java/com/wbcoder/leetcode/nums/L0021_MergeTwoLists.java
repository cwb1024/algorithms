package com.wbcoder.leetcode.nums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class L0021_MergeTwoLists {

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        //构建虚拟节点，用作链表的返回值
        ListNode virtual = new ListNode(0);

        //一些边界数值的判断
        if (l1 == null) return l2;
        if (l2 == null) return l1;

        //动态指针节点
        ListNode sortNode = virtual;

        while (l1 != null && l2 != null) {
            //把节点绑定到动态指针上，并且把当前节点往后移动，所有的关联已经关联到了动态指针上
            if (l1.val < l2.val) {
                sortNode.next = l1;
                sortNode = sortNode.next;
                l1 = l1.next;
            } else {
                sortNode.next = l2;
                sortNode = sortNode.next;
                l2 = l2.next;
            }
        }
        //上面的循环退出后，某个链表已经走完了
        if (l1 != null) sortNode.next = l1;
        if (l2 != null) sortNode.next = l2;

        return virtual.next;
    }


    public ListNode mergeTwoLists1(ListNode l1, ListNode l2) {
        //构建虚拟节点，用作链表的返回值
        ListNode virtual = new ListNode(0);

        //一些边界数值的判断
        if (l1 == null) return l2;
        if (l2 == null) return l1;

        //动态指针节点
        ListNode sortNode = virtual;

        while (l1 != null && l2 != null) {
            //把节点绑定到动态指针上，并且把当前节点往后移动，所有的关联已经关联到了动态指针上
            if (l1.val < l2.val) {
                sortNode.next = l1;
                l1 = l1.next;
                sortNode = sortNode.next;
            } else {
                sortNode.next = l2;
                l2 = l2.next;
                sortNode = sortNode.next;
            }
        }
        //上面的循环退出后，某个链表已经走完了
        if (l1 != null) sortNode.next = l1;
        if (l2 != null) sortNode.next = l2;

        return virtual.next;
    }


    public static void main(String[] args) {
//        int a = 10;
//
//        if(a>0)
//        {
//            System.out.println("a > 0");
//
//        } else if (a < 11) {
//
//            System.out.println("a < 11");
//        }else {
//            System.out.println(" a ..");
//        }

        test();

    }

    public static void StringBuild(StringBuilder sb){
        sb.append("xx");
    }

    public static void test() {
        StringBuilder sb = new StringBuilder();
        sb.append("aa");

        StringBuild(sb);

//        List<int[]> res = new ArrayList<>();
//        res.add(new int[]{left[0][0],intervals[i-1][1]});
        System.out.println(sb.toString());

        int[][] intervals = new int[][]{{1, 4}, {0, 2}, {3, 5}};
        List<int[]> res = new ArrayList();
        List<int[]> toList = Arrays.asList(intervals);
        List<int[]> newList = new ArrayList(toList);
        newList.sort((o1, o2) -> o1[0] - o2[0]);
        System.out.println(newList);


        newList.sort((o1, o2) -> o1[0] - o2[0]);


        System.out.println(Arrays.deepToString(intervals));


    }
}
