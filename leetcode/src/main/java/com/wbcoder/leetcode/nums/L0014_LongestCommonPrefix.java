package com.wbcoder.leetcode.nums;

public class L0014_LongestCommonPrefix {


    /**
     * 正向判断
     *
     * @param strs
     * @return
     */
    public String longestCommonPrefix1(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }

        String res = "";
        int index = 0;
        boolean bool = false;
        while (true) {
            char tmp = 0;
            //内层循环，对三个字符串进行字符的比较并且进行比较
            for (int i = 0; i < strs.length; i++) {
                //如果越界了，这里就停止
                if (index >= strs[i].length()) {
                    bool = true;
                    break;
                }
                //把第一个前缀放进去
                if (i == 0) {
                    tmp = strs[i].charAt(index);
                }
                //不是第一个这里就进行比较
                if (strs[i].charAt(index) != tmp) {
                    bool = true;
                    break;
                }
            }

            if (bool) {
                break;
            }

            index++;
            res += tmp;
        }
        return res;

    }


    /**
     * 逆向判断
     *
     * @param strs
     * @return
     */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        String res = strs[0];
        for (String s : strs) {
            while (!s.startsWith(res)) {
                if (res.length() == 1) {
                    return "";
                }
                res = res.substring(0, res.length() - 1);
            }
        }
        return res;
    }


    public static void main(String[] args) {
        String[] strs = {"flower", "flow", "flight"};
        L0014_LongestCommonPrefix leetcode_14LongestCommonPrefix = new L0014_LongestCommonPrefix();
        System.out.println(leetcode_14LongestCommonPrefix.longestCommonPrefix(strs));
    }
}
