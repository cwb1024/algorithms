package com.wbcoder.leetcode.nums;

import java.util.Arrays;

/**
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 * <p>
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 * <p>
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-element
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class L0027_RemoveElement {

    public int removeElement(int[] nums, int val) {
        if (nums == null) {
            return 0;
        }
        int right = nums.length - 1;

        //这里进行一次遍历，如果出现重复的就把这个位置与最后的位置进行交换，并且把后面的位置指针向前移动，直到左右指针相遇

        for (int i = 0; i <= right; i++) {
            //这里防止交换之后仍然是重复的，这里进行循环判断
            while (nums[i] == val && i <= right) {
                int tmp = nums[i];
                nums[i] = nums[right];
                nums[right--] = tmp;
            }
        }
        return right + 1;
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 3};
        L0027_RemoveElement leetcode_27RemoveElement = new L0027_RemoveElement();
        System.out.println(leetcode_27RemoveElement.removeElement(nums, 2));
        System.out.println(Arrays.toString(nums));
    }
}
