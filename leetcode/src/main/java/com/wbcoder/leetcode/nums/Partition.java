package com.wbcoder.leetcode.nums;

public class Partition {
    public ListNode partition(ListNode head, int x) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode p1 = dummyHead;
        ListNode p2 = dummyHead;
        ListNode p3 = dummyHead.next;

        //摘小的，放到p1指针后边
        while(p3 != null)
        {
            if(p3.val<x)
            {
                //摘掉p3,并且复原p2,p3的相对关系
                ListNode cur = p3;
                p2.next = p3.next;
                p3 = p2.next;

                //摘下来的p3,放到p1后边
                cur.next = p1.next;
                p1.next = cur;
                p1 = p1.next;

                 if(p2.next != p3)
                 {
                     p2 = p2.next;
                 }
            }else
            {
                p2 = p2.next;
                p3 = p2.next;
            }
        }
        return dummyHead.next;
    }

    public ListNode buildLinkedList(int[] arr) {
        if(arr == null || arr.length == 0) return null;
        ListNode dummyHead = new ListNode(-1);
        ListNode cur = dummyHead;
        for (int i = 0; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
        return dummyHead.next;
    }


    public static void main(String[] args) {

        Partition pp = new Partition();

        //构建链表
        int[] arr = new int[]{
                1,1
        };
        int x = 2;
        ListNode head = pp.buildLinkedList(arr);
        pp.partition(head, x);

    }
}
