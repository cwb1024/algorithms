package com.wbcoder.leetcode.nums;

import java.util.Arrays;

public class L0088_Solution88 {

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        //从后往前遍历，防止覆盖

        //第一个数组指针i,第二个数组指针j,最终数组k
        int i = m - 1, j = n - 1, k = m + n - 1;

        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[k--] = nums1[i--];
            } else {
                nums1[k--] = nums2[j--];
            }
        }

        while (j >= 0) {
            nums1[k--] = nums2[j--];
        }
    }

    public static void main(String[] args) {
        int[] num1 = {1, 2, 3, 6, 7, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] num2 = {4, 5, 6, 7, 8, 9, 10, 11};
        L0088_Solution88 solution88 = new L0088_Solution88();
        solution88.merge(num1, 5, num2, 8);
        System.out.println(Arrays.toString(num1));
    }
}
