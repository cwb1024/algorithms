package com.wbcoder.leetcode.nums;

import java.util.*;

public class L0003_Solution03 {

    public int lengthOfLongestSubstring(String s) {
        int num = 1;
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            //每一个串的开始
            for (int j = i + 1; j < s.length(); j++) {

                char charTmp = s.charAt(j);
                boolean b = true;

                //这个串的开始，判断下一个位置是否可用，跟下一个位置之前的都比较
                for (int k = i; k < j; k++) {

                    char charStart = s.charAt(k);
                    //从串的开始一个一个比较
                    if (charStart == charTmp) {
                        b = false;
                        break;
                    }

                }
                if (b) {

                    num++;

                } else {
                    break;
                }
            }

            sum = sum > num ? sum : num;
            num = 1;
        }
        return sum;
    }

    public static int lengthOfLongestSubstring1(String s) {
        int maxLength = 0;//过程中记录了是否是最大值
        char[] chars = s.toCharArray();
        int leftIndex = 0;//起始值
        //需要比较的位置，管着高位
        for (int i = 0; i < chars.length; i++) {
            //内层就是一个循环比较的过程，从起始位置比较，这个起始位置什么时候会变，遍历过程遇到重复的时候，就会变
            for (int innerIndex = leftIndex; innerIndex < i; innerIndex++) {
                if (chars[innerIndex] == chars[i]) {
                    maxLength = maxLength > i - leftIndex ? maxLength : i - leftIndex;
                    leftIndex = innerIndex + 1;
                    break;
                }
            }
        }
        return Math.max(chars.length - leftIndex, maxLength);
    }


    public int lengthOfLongestSubstring3(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int max = 0;
        //起始值
        for (int leftIndex = 0; leftIndex < s.length(); leftIndex++) {


            int tmpIndex = leftIndex;

            //这样比的是相邻的两位

            for (int endIndex = tmpIndex + 1; endIndex < s.length(); endIndex++) {
                if (s.charAt(endIndex) != s.charAt(tmpIndex)) {
                    //外层带比较的页迭代进位
                    tmpIndex++;
                } else {
                    //遇到相同的了，就进行赋值比较操作
                    max = tmpIndex > max ? tmpIndex : max;
                    break;
                }
            }
        }
        return max;
    }

    public int lengthOfLongestSubstring4(String s) {
        if (s == null || s.length() == 0) return 0;

        Deque deque = new ArrayDeque();

        HashSet<Character> set = new HashSet<Character>();

        int left = 0, right = 0;

        int res = 0;

        while (right < s.length()) {
            while (right < s.length() && !set.contains(s.charAt(right))) {
                set.add(s.charAt(right));
                right++;
            }
            res = Math.max(res, right - left);

            while (right < s.length() && set.contains(s.charAt(right))) {
                set.remove(s.charAt(left));
                left++;
            }
        }
        return res;
    }

    public int nextGreaterElement(int num) {
        String s = String.valueOf(num);
        char[] arr = s.toCharArray();
        int[] nums = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            nums[i] = arr[i] - '0';
        }
        int n = nums.length;
        int i = n - 2, j = n - 1, k = n - 1;
        while (i >= 0 && nums[i] >= nums[j]) {
            i--;
            j--;
        }
        if (i >= 0) {
            while (j <= k && nums[k] <= nums[i]) {
                k--;
            }
            swap(nums, i, k);
        }
        Arrays.sort(nums, j, n);
        int res = 0;
        for (int t = n - 1; t >= 0; t--) {
            res += (int) (nums[t] * Math.pow(10, n - 1 - t));
        }
        return res;
    }


    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


    public int[] dailyTemperatures(int[] arr) {
        int n = arr.length;
        int[] ans = new int[n];
        int i = 0, j = 1;

        int[] arr_0 = new int[0];
        int[] arr_1 = new int[1];
        if (n == 0) return arr_0;
        if (n == 1) return arr_1;

        while (i < n && j < n) {
            if (arr[i] >= arr[j]) {
                j++;
            }
            //如果出现没找到最大的情况，结束
//                if(j == n-1 && (arr[i]>=arr[j])) break;
            if (i < n && j < n && arr[i] < arr[j]) {
                ans[i] = j - i;
                //找到设置结果，并且初始化 i，j相对位置
                i++;
                j = i + 1;
                continue;
            }
        }
        return ans;
    }

    public int sumNumbers(TreeNode root) {
        List<String> nums = new ArrayList();
        dfs(root, nums, "");
        return calculate(nums);
    }

    public void dfs(TreeNode root, List<String> res, String appendVal) {
        if (root == null) {
            return;
        }
        appendVal += root.val;
        dfs(root.left, res, appendVal);
        dfs(root.right, res, appendVal);
        //找到叶子节点
        if (root.left == null && root.right == null) {
            if (!appendVal.isEmpty()) {
                res.add(appendVal);
            }
            return;
        }
        return;
    }

    public int calculate(List<String> strs) {
        int res = 0;
        for (String s : strs) {
            char[] chars = s.toCharArray();
            int val = 0;
            for (int i = chars.length - 1; i >= 0; i--) {

                val += (chars[i]-'0') * Math.pow(10, chars.length - 1 - i);
            }
            res += val;
        }
        return res;
    }

    public TreeNode buildTree() {
        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(2);
        TreeNode right = new TreeNode(3);
        root.left = left;
        root.right = right;
        return root;
    }

    public static void main(String[] args) {
        L0003_Solution03 solution01 = new L0003_Solution03();
        int x = solution01.sumNumbers(solution01.buildTree());
        System.out.println(x);
    }
}
