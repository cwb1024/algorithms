package com.wbcoder.leetcode.nums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class L0002_Solution02 {

    public static int[] twoSum1(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    int[] arr = {i, j};
                    return arr;
                }
            }
        }
        return null;
    }

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target-nums[i])) {
                int[] arr = {i, map.get(target - nums[i])};
                return arr;
            }
            map.put(nums[i], i);
        }
        return null;
    }


    //for while 不一定就是n^2，可能只是让条件继续进行下去
    public static int[] twoSum2(int [] nums,int target) {
        for (int j = 0, i = nums.length - 1; j < nums.length; j++) {
            if (i - 1 > j && nums[i - 1] + nums[j] >= target) {
                i--;
            }
            if (nums[i] + nums[j] == target) {
                return new int[]{i, j};
            }
        }
        return new int[]{-1, -1};
    }

    /**
     * @description: 排好序的情况
     * @author: chengwb
     * @Date: 2020-03-07 21:54
     */
    public static int[] sortedTwoSum(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
            int sum = nums[left] + nums[right];
            if (sum < target) {
                left++;
            } else if (sum > target) {
                right--;
            } else {
                return new int[]{left, right};
            }
        }
        return new int[]{-1, -1};
    }


    /**
      * @description: 未排序的情况
      * @author: chengwb
      * @Date: 2020-03-07 21:58
      */
    public static int[] unsortedTwoSum(int[] nums, int target) {
        HashMap<Integer,Integer> map = new HashMap();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{i,map.get(target-nums[i])};
            }
            map.put(nums[i], i);
        }
        return new int[]{-1, -1};
    }

    public static void main(String[] args) {
        int[] ar = {1,4,5,7};
        System.out.println(Arrays.toString(twoSum2(ar, 6)));
    }
}
