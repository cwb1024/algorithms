package com.wbcoder.leetcode.nums;

/**
 * @description: 反转链表
 * @author: chengwb
 * @Date: 2020/6/25 12:06
 */
public class ReverseList {

//    public ListNode reverseList(ListNode head) {
//        ListNode pre = null;
//        ListNode cur = head;
//        while (cur != null)
//        {
//            ListNode tmp = cur.next;
//            cur.next = pre;
//            pre = cur;
//            cur = tmp;
//        }
//        return pre;
//    }


//    public ListNode reverseList(ListNode head) {
//        ListNode pre = null;
//        ListNode cur = head;
//        while (cur != null) {
//            ListNode tmp = cur.next;
//            cur.next = pre;
//            pre = cur;
//            cur = tmp;
//        }
//        return pre;
//    }

    /**
     * 递归实现翻转链表
     *
     * @param node
     * @return
     */
    public ListNode reverseList(ListNode node) {
        if (node.next == null) {
            return node;
        }
        ListNode reverseNode = reverseList(node.next);
        node.next.next = node;
        node.next = null;
        return reverseNode;
    }
}


