package com.wbcoder.leetcode.nums;

public class L0003_LengthOfLongestSubstring {

    public static int lengthOfLongestSubstring(String s) {
        if(s == null || s.length() >0)
        {
            return 0;
        }

        int l = 0 , r = 0;
        int res = 0;
        for(;r < s.length(); r++)
        {
            for(int i = l;i<r;i++)
            {
                if(s.charAt(i) == s.charAt(r))
                {
                    l = i+1;
                    break;
                }
            }
            if(r -l +1 > res) res = r -l + 1;
        }
        return res;
    }

    public static void main(String[] args) {
        lengthOfLongestSubstring("abcabcbb");
    }
}

