package com.wbcoder.leetcode.nums;

public class L0009_Solution09 {

    public boolean isPalindrome(int x) {
        String s = String.valueOf(x);
        char[] chars = s.toCharArray();
        boolean bool = false;
        int left = 0;
        int right = chars.length - 1;
        while (left <= right) {
            if (chars[left++] == chars[right--]) {
                bool = true;
            } else {
                return false;
            }
        }
        return bool;
    }

    public static void main(String[] args) {
        L0009_Solution09 solution09 = new L0009_Solution09();

        System.out.println(solution09.isPalindrome(-10));
    }
}
