package com.wbcoder.leetcode.nums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
  * @description: 字符串全排列
  * @author: chengwb
  * @Date: 2020/8/31 22:41
  */
public class StringPermutation {

    List<String> res;



    public String[] permutation(String s) {

        if (s == null || s.length() == 0) {
            return new String[]{};
        }

        char[] chars = s.toCharArray();

        StringBuilder path = new StringBuilder();

        List<String> res = new ArrayList<>();

        dfs(res, path, chars);

        return res.toArray(new String[0]);
    }

    //结果集、可用的字符
    void dfs(List<String> res, StringBuilder path, char[] chars) {

        if (chars.length == 0) {

            res.add(path.toString());

            return;
        }

        for (int i = 0; i < chars.length; i++) {

            //尝试使用结果
            path.append(chars[i]);
            //进行递归
            dfs(res, path, removeElement1(chars, chars[i]));
            //恢复初始状态
            path.deleteCharAt(path.length() - 1);
        }

    }

    //这里顺序被打乱，这里数据数据往前挪动，有效数据往前移动
    int removeElement(char[] chars, char c) {
        if (chars == null || chars.length == 0) {
            return 0;
        }
        int r = chars.length - 1;
        for (int i = 0; i <= r; i++) {
            if (chars[i] == c) {
                char tmp = chars[i];
                chars[i--] =chars[r];
                chars[r--] = tmp;
            }
        }
        return r + 1;
    }

    /**
     * 原地删除一个数据，并且保证数组内原来数据的
     * @param chars
     * @param c
     * @return
     */
    int removeElement0(char[] chars, char c) {
        if (chars == null || chars.length == 0) {
            return 0;
        }
        int n = chars.length;
        int l = 0, r = 0;
        //这里双指针，l指向准备写入的有效位置，r指向遍历的位数
        while (l <= r && r < n) {
            //遇到重复的跳过
            if (chars[r] == c) {
                r++;
            } else {
                chars[l++] = chars[r++];
            }
        }
        return l;
    }

    /**
     * 返回移除后元素的指定数组内容
     * @param chars
     * @param c
     * @return
     */
    char[] removeElement1(char[] chars, char c) {
        //这里构建一个不能改变原来数组内元素顺序的数组，重新构建一个数组回去
        if (chars == null || chars.length == 0) {
            return chars;
        }
        int l = 0, r = 0, n = chars.length;
        while (l <= r && r < n) {
            if (chars[r] == c) {
                r++;
            } else {
                chars[l++] = chars[r++];
            }
        }
        char[] res = new char[l];
        System.arraycopy(chars, 0, res, 0, l);
        return res;
    }

    public static void main(String[] args) {
        String s = "abc";

        StringPermutation stringPermutation = new StringPermutation();

        String[] permutation = stringPermutation.permutation(s);

        System.out.println(Arrays.deepToString(permutation));

        Integer integer = Integer.valueOf(s);


    }

}
