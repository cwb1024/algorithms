package com.wbcoder.leetcode.nums;

import java.util.Arrays;

public class FindKthMaxNums {

    /**
     * 直接排序处理
     *
     * @param arr
     * @param k
     * @return
     */
    public int findKth0(int[] arr, int k) {
        Arrays.sort(arr);
        return arr[arr.length - k + 1];
    }

    /**
     * 快速查找找到第K大
     *
     * @param arr
     * @param k
     * @return
     */
    public int findKth(int[] arr, int k) {
        int partition = partition(arr, 0, arr.length);

        while (partition != k - 1) {
            if (partition < k - 1) {
                partition = partition(arr, partition + 1, arr.length);
            } else if (partition > k - 1) {
                partition = partition(arr, 0, partition);
            }
        }
        return arr[partition];
    }

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public int partition(int[] arr, int l, int r) {
        int i = l;
        int j = l + 1;
        int pivot = arr[i];
        for (; j < r; j++) {
            if (arr[j] > pivot) {
                swap(arr, i + 1, j);
                i++;
            }
        }
        arr[l] = arr[i];
        arr[i] = pivot;
        return i;
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 5, 4, 3};
        FindKthMaxNums ff = new FindKthMaxNums();
//        int kth = ff.findKth(arr, 2);
        int kth = ff.findKthByHeap(arr, 2);
        System.out.println(kth);
    }


    /**
     * 通过建立小顶堆来查找第K大
     *
     * @param arr
     * @param k
     * @return
     */
    public int findKthByHeap(int[] arr, int k) {
        buildKthHeap(arr, k);

        int n = arr.length;
        for (int i = k; i < n; i++) {
            if (arr[0] > arr[i]) continue;

            swap(arr, 0, i);
            adjust_heap(arr, 0, k);
        }

        return arr[0];
    }

    private void buildKthHeap(int[] arr, int k) {
        heapify(arr, k);
    }

    private void heapify(int[] arr, int k) {
        for (int i = (k >> 1) - 1; i >= 0; i--) {
            adjust_heap(arr, i, k);
        }
    }

    private void adjust_heap(int[] arr, int parent, int n) {
        int half = n >> 1;
        while (parent < half) {
            int child = (parent << 1) + 1;
            int right = child + 1;
            if (right < n && right < child) {
                child = right;
            }
            if (arr[child] < arr[parent]) {
                swap(arr, child, parent);
            } else {
                break;
            }
            parent = child;
        }
    }
}
