package com.wbcoder.leetcode.nums;

/**
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 */
public class L0053_maxSubArray {


    /**
     * 利用DP的解法
     * <p>
     * 第一步：构建状态集合 f[i] 属性最大值
     * 第二步：求出一个状态集合
     * f[j]=max{f[i-1][j],f[i][j]}
     *
     * @param nums
     * @return
     */
    public int maxSubArray(int[] nums) {

        if (nums == null) return 0;

        int res = Integer.MIN_VALUE;
        int f_n = -1;
        for (int i = 0; i < nums.length; i++) {
            f_n = Math.max(nums[i], f_n + nums[i]);

            res = Math.max(res, f_n);
        }

        return res;
    }


    public static void main(String[] args) {
        int[] array = {-1};
        L0053_maxSubArray leetcode_53maxSubArray = new L0053_maxSubArray();
        System.out.println(leetcode_53maxSubArray.maxSubArray(array));
    }
}
