package com.wbcoder.leetcode.nums;

import java.util.HashMap;

public class L0146_LRUCache {

    public class DLinkNode{
        int key;
        int val;
        DLinkNode prev;
        DLinkNode next;

        public DLinkNode() {}
        public DLinkNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    private HashMap<Integer, DLinkNode> cache;
    private int capacity;
    private int size;
    private DLinkNode head, tail;

    public L0146_LRUCache(int capacity) {
        this.capacity = capacity;
        size = 0;
        cache = new HashMap<>();
        head = new DLinkNode();
        tail = new DLinkNode();
        head.next = tail;
        tail.prev = head;
    }

    private int get(int key) {
        DLinkNode node = cache.get(key);
        if (node == null) return -1;
        moveToHead(node);
        return node.val;
    }

    private void put(int key, int value) {
        DLinkNode node = cache.get(key);
        if (node == null) {
            DLinkNode newNode = new DLinkNode(key, value);
            size++;
            addToHead(newNode);
            cache.put(key, newNode);
            if (size > capacity) {
                DLinkNode tail = removeTail();
                cache.remove(tail.key);
                size--;
            }
        } else {
            node.val = value;
            moveToHead(node);
        }
    }

    private void addToHead(DLinkNode node) {
        node.prev = head;
        node.next = head.next;
        head.next.prev = node;
        head.next = node;
    }

    private void removeNode(DLinkNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void moveToHead(DLinkNode node) {
        removeNode(node);
        addToHead(node);
    }

    private DLinkNode removeTail() {
        DLinkNode node = tail.prev;
        removeNode(node);
        return node;
    }

    public static void main(String[] args) {
        L0146_LRUCache lru = new L0146_LRUCache(2);
        lru.put(1, 1);
        lru.put(2, 2);
        lru.put(3, 3);
        lru.put(4, 4);
    }
}
