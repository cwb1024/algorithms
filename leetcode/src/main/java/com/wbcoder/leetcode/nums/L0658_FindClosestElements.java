package com.wbcoder.leetcode.nums;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class L0658_FindClosestElements {

    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        //1、在数据内查找大于等于x的最小值,这里是找到上界
        int l = 0, r = arr.length - 1;
        while (l < r) {
            int mid = l + r >> 1;
            if (arr[mid] >= x)
                r = mid;
            else
                l = mid + 1;
        }

        l--;

        while (r - l - 1 < k && (l >= 0 || r < arr.length)) {
            if (l >= 0 && r < arr.length) {
                if (x - arr[l] <= arr[r] - x)
                    l--;
                else
                    r++;
            } else if (l >= 0)
                l--;
            else
                r++;
        }
        //返回结果
        List<Integer> res = new ArrayList<Integer>();
        for (int i = l + 1; i < r; i++) {
            res.add(arr[i]);
        }
        return res;
    }


    public static void main(String[] args) {

        int[] arr = {1, 1, 1, 10, 10, 10};


        L0658_FindClosestElements leetcode_658FindClosestElements = new L0658_FindClosestElements();


        System.out.println(leetcode_658FindClosestElements.findClosestElements(arr, 1, 9));

        List<Integer> res = new ArrayList();
        Iterator<Integer> iterator = res.iterator();
        while (iterator.hasNext()) {

        }

    }
}
