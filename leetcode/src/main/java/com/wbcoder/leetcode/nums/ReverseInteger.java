package com.wbcoder.leetcode.nums;

public class ReverseInteger {

    public int reverse(int x) {

        if (x == Integer.MAX_VALUE) return 0;

        int neg = x < 0 ? -1 : 1;

        x *= neg;

        int ret = 0;

        //这里循环需要对每一位移位操作
        while (x > 0) {
            int n = ret;
            //左移位
            n *= 10;
            //加上余数
            n += x % 10;
            //舍弃余数
            x /= 10;

            if (n / 10 != ret) return 0;

            ret = n;

        }
        return ret * neg;
    }

    public static void main(String[] args) {
        ReverseInteger reverseInteger = new ReverseInteger();
        System.out.println(reverseInteger.reverse(-321));
    }
}
