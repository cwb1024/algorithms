package com.wbcoder.leetcode.nums;

/**
 * @description: 移除链表内的重元素
 * @author: chengwb
 * @Date: 2020/6/25 12:15
 */
public class RemoveLinkListElements {

    public ListNode removeElements(ListNode head, int val) {

        //要删除的元素可能是头节点，这个增加一个虚拟节点指向头节点，减少判断
        ListNode virtual = new ListNode(0);
        virtual.next = head;

        //动态指针一直走，去判断，去维护这个关系
        ListNode cur = virtual;
        // [-1] --> 0 --> 1 --> 2 --> 3
        while (cur.next != null) {

            if (cur.next.val == val) {
                //这个操作会把我们找到相等的节点的关联关系删除掉
                cur.next = cur.next.next;
            } else {
                //否则当前节点下移
                cur = cur.next;
            }
        }

        return virtual.next;
    }

}
