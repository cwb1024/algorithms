package com.wbcoder.leetcode.nums;

import java.util.LinkedHashMap;
import java.util.Map;

public class L0146_LRUCache2 extends LinkedHashMap {

    private int capacity;

    public L0146_LRUCache2(int capacity) {
        super(capacity, 0.75F, true);
        this.capacity = capacity;
    }

    @Override
    public Object get(Object key) {
        return super.getOrDefault(key,-1);
    }

    @Override
    public Object put(Object key, Object value) {
        return super.put(key, value);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > capacity;
    }


    public static void main(String[] args) {
        L0146_LRUCache2 lru = new L0146_LRUCache2(2);
        lru.put(1, 1);
        lru.put(2, 2);
        lru.put(3, 3);
        lru.put(4, 4);

    }
}
