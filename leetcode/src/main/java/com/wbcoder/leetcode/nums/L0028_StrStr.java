package com.wbcoder.leetcode.nums;

/**
 * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。
 * 如果不存在，则返回  -1。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/implement-strstr
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class L0028_StrStr {

    public int strStr(String haystack, String needle) {
        if ("".equals(haystack) && "".equals(needle)) {
            return 0;
        }
        int res = -1;
        int needleLength = needle.length();
        if (needleLength == 0) {
            return 0;
        }
        for (int i = 0; i < haystack.length(); i++) {

            if (haystack.charAt(i) == needle.charAt(0)) {
                int right = i + needleLength;
                if (right <= haystack.length() &&
                        haystack.substring(i, right).equals(needle)) {
                    res = i;
                    break;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        L0028_StrStr leetcode_28StrStr = new L0028_StrStr();
        System.out.println(leetcode_28StrStr.strStr("a", ""));

    }
}
