package com.wbcoder.leetcode.nums;

/**
 * @description: 判断链表环的入口节点
 * @author: chengwb
 * @Date: 2020/6/26 16:43
 */
public class L0022_DetectCycle {

    class Solution {
        public ListNode detectCycle(ListNode head) {
            ListNode fast = head;
            ListNode slow = head;
            while (fast != slow) {
                //快指针先走
                if (fast == null || fast.next == null) {
                    return null;
                }
                fast = fast.next;
                if (fast == null) {
                    return null;
                }
                fast = fast.next;
                slow = slow.next;
            }
            fast = head;
            while (fast != slow) {
                fast = fast.next.next;
                slow = slow.next;
            }
            return slow;
        }
    }
}
