package com.wbcoder.leetcode.nums;

public class L0005_Solution05 {

    /**
     * 动态规划方法
     */
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();
        boolean[][] isPalindrome = new boolean[n][n + 1];

        return null;
    }






    /**
     * 普通解法
     * @param s
     * @return
     */
    public String longestPalindrome2(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();
        String max = "";
        //这个词的起始位置
        for (int i = 0; i < n; i++) {
            //从起始位置，一直往后找，并且判断是否首回文
            for (int j = i; j < n; j++) {
                if (isPalindrome(s.substring(i, j + 1)) && j - i + 1 > max.length()) {
                    //如果找到最大的就替换
                    max = s.substring(i, j + 1);
                }
            }
        }
        return max;
    }

    private boolean isPalindrome(String s) {
        String d = new StringBuffer(s).reverse().toString();
        return s.equals(d);
    }


    public static void main(String[] args) {
        L0005_Solution05 solution05 = new L0005_Solution05();
        System.out.println(solution05.longestPalindrome("abbc"));
    }

}