package com.wbcoder.leetcode.nums;

import java.util.ArrayList;

public class L0001_Solution01 {

    public int[][] findContinuousSequence(int target) {
        //外层记录开始的位置
        ArrayList<int[]> result = new ArrayList<int[]>();
        for (int i = 1; i <= target; i++) {
            //内层从初始的位置往后遍历，超过了，或者不等于就结束这次值
            int tmp = 0;
            int end = -1;
            for (int j = i; j <= target; j++) {
                tmp += j;
                if (tmp > target) {
                    break;
                }
                if (tmp == target) {
                    end = j;
                    break;
                }
            }
            if (end != -1 && end - i >= 1) {
                int[] tmpArray = new int[end - i + 1];
                int z = 0;
                for (int a = i; a <= end; a++) {
                    tmpArray[z++] = a;
                }
                result.add(tmpArray);
            }
        }
        return result.toArray(new int[result.size()][]);
    }

    public static void main(String[] args) {
        L0001_Solution01 solution01 = new L0001_Solution01();
        solution01.findContinuousSequence(9);
    }
}
