package com.wbcoder.leetcode.nums;

import java.util.Arrays;

public class L0066_plusOne {

    public static int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            digits[i] = (digits[i] + 1) % 10;
            if (digits[i] != 0) {
                break;
            }
        }
        if (digits[0] == 0) {
            int[] ints = new int[digits.length + 1];
            ints[0] = 1;
            return ints;
        }
        return digits;
    }


    public static void main(String[] args) {
        int[] arr = {9, 9};
        int[] ints = plusOne(arr);
        System.out.println(Arrays.toString(ints));
    }
}
