package com.wbcoder.leetcode.nums;

import java.util.ArrayList;
import java.util.List;

public class RestoreIpAddresses {


    public List<String> restoreIpAddresses(String s) {
        List<String> res = new ArrayList();
        if(s == null || s.length() < 4 || s.length() > 12)
        {
            return res;
        }

        dfs(0, 0, s, res, "");

        return res;

    }

    /**
     *
     * @param deep 深度
     * @param start 本层开始索引
     * @param s 初始字符串
     * @param res 统计结果
     * @param path 累计字符
     */
    void dfs(int deep,int start,String s,List<String> res,String path)
    {
        if(deep == 4)
        {
            if(start == s.length())
            {
                res.add(path);
            }
            return;
        }

        for(int i = 1;i<=3;i++)
        {
            if(start + i > s.length()) return;
            String tmp = s.substring(start,start+i);

            if(!isVaild(tmp)) continue;

            //提取现场
            String tt = path;

            //这里构建path
            path += tmp;
            if(start + i != s.length()) path += '.';

            dfs(deep + 1, start + i, s, res, path);
            //恢复现场
            path = tt;
        }
    }

    boolean isVaild(String s)
    {
        //null or 只有一个 不用进行判断，直接可用
        if(s == null || s.length() <=1) return true;

        //长度超过三个，不考虑
        if(s.length() > 3) return false;

        //如果长度超过一个，但是确实是以'0'开头的，这里是不允许的
        if(s.length() > 1 && s.charAt(0) == '0') return false;

        //进行数值范围的判读
        Integer intValue = Integer.valueOf(s);
        if(intValue > 255) return false;
        return true;
    }


    public static void main(String[] args) {

//        RestoreIpAddresses ip = new RestoreIpAddresses();
//
//        List<String> strings = ip.restoreIpAddresses("25525511135");
//
//        System.out.println(strings);

//        StringBuilder sb = new StringBuilder();

//        sb.append("abcdefghijklmnopqrstuvwxyz");

//        sb.deleteCharAt(sb.length() - 1);

//        System.out.println(sb.toString());

//        int[] a = new int[]{};

//        System.out.println(Arrays.deepToString(a));

        int [] a = new int[]{1};

        int i = removeElement(a, a.length, 1);

        System.out.println(i);

        List<Integer> list = new ArrayList<>();

    }

    static int removeElement(int[] u,int un,int val)
    {
        if(u == null || u.length ==0)
        {
            return 0;
        }
        int l =0, r=0,n=un;
        for(int i =0;i<n;i++)
        {
            if(u[i]==val)
            {
                r++;
            }else
            {
                u[l++] = u[r++];
            }
        }
        return l;
    }


}
