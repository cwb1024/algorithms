package com.wbcoder.leetcode.nums;

public class SwapKListNode {
    public ListNode swapListNode(ListNode head, int k) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode right = dummyHead;
        ListNode left = dummyHead;
        while (--k > 0) {
            right = right.next;
        }
        ListNode leftPre = right;
        while(right != null) {
            right = right.next;
            left = left.next;
        }
        ListNode rightPre = left;
        int val = leftPre.next.val;
        leftPre.next.val = rightPre.next.val;
        rightPre.next.val = val;
        return dummyHead.next;
    }


    public ListNode swapListNode1(ListNode head, int k) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode right = dummyHead;
        ListNode left = dummyHead;
        while (--k > 0) {
            right = right.next;
        }
        ListNode leftPre = right;
        while(right != null) {
            right = right.next;
            left = left.next;
        }
        ListNode rightPre = left;

        ListNode leftNext = leftPre.next.next;
        ListNode rightNext = rightPre.next.next;

        return dummyHead.next;
    }
}
