package com.wbcoder.leetcode.nums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * dulidong@didichuxing.com对所有人说 (20:18)
 * 给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
 *
 * 注意：答案中不可以包含重复的三元组。
 *
 *
 *
 * 示例：
 *
 * 给定数组 nums = [-1, 0, 1, 2, -1, -4]，
 *
 * 满足要求的三元组集合为：
 * [
 * [-1, 0, 1],
 * [-1, -1, 2]
 * ]
 */

public class ThreeSum {

    public static List<List<Integer>> find(int[] arr){
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(arr);
        for(int i =0;i<arr.length;i++){
            if(arr[i]>0) break;
            if(i>0 && arr[i] == arr[i-1]) continue;
            int l = i+1;
            int r = arr.length-1;


            while(l<r){
                int sum = arr[i] + arr[l] + arr[r] ;
                if(sum == 0){
                    res.add(Arrays.asList(arr[i],arr[l],arr[r]));

                    while(l<r && arr[l] == arr[l+1]) l++;
                    while(l<r && arr[r] == arr[r-1]) r--;

                    l++;
                    r--;
                }else if(sum < 0){
                    l++;
                }else if(sum > 0){
                    r--;
                }
            }


        }
        return res;
    }

    public static void main(String[] args) {
        int[] arr = {-1, 0, 1, 2, -1, -4};
        List<List<Integer>> lists = find(arr);
        System.out.println(lists);
    }
}
