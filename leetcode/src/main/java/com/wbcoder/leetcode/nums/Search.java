package com.wbcoder.leetcode.nums;

public class Search {


    public int search(int[] nums, int target) {

        if (nums.length == 0) return -1;

        int l = 0 , r = nums.length -1;
        //找到最小值
        while(l < r)
        {
            int mid = l + r >>1;
            if(nums[mid] <= nums[nums.length-1])
                r = mid;
            else
                l = mid + 1;
        }
        //确定区间
        if(target <= nums[nums.length-1]){
            //这里就是在右边的区间里边
            r = nums.length -1;
        }else{
            //否则就是在左边的区间里边
            l = 0;
            r--;
        }
        //二分去找这个值
        while(l < r)
        {
            int mid = l + r >> 1;
            //二分找这个值
            if(nums[mid] >= target){
                r = mid;
            }else{
                l = mid + 1;
            }
        }
        if(nums[l] == target) return l;

        return -1;
    }

    public static void main(String[] args) {

        int [] array = {3,1};

        Search leetCode = new Search();
        System.out.println(leetCode.search(array, 1));
    }
}
