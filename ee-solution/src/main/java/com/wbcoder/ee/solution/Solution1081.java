package com.wbcoder.ee.solution;

import java.util.*;

public class Solution1081 {

    Map<Character, char[]> mapping;

    void initMapping() {
        mapping = new HashMap<>();
        mapping.put('2', new char[]{'a', 'b', 'c'});
        mapping.put('3', new char[]{'d', 'e', 'f'});
        mapping.put('4', new char[]{'g', 'h', 'i'});
        mapping.put('5', new char[]{'j', 'k', 'l'});
        mapping.put('6', new char[]{'m', 'n', 'o'});
        mapping.put('7', new char[]{'p', 'q', 'r', 's'});
        mapping.put('8', new char[]{'t', 'u', 'v'});
        mapping.put('9', new char[]{'w', 'x', 'y', 'z'});
    }

    List<String> res = new ArrayList<>();
    StringBuilder ans = new StringBuilder();

    int a = 1;

    void f1() {
        int b = a;
        int c = 100;
        a = Math.max(b, c);
        System.out.println(a);
    }

    void f2() {
        int c = 100;
        a = Math.max(a, c);
        System.out.println(a);
    }

    void test() {
        f1();
        f2();
    }

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("12345");
        sb.deleteCharAt(4);
        System.out.println(sb.toString());

        List<Integer> arr = new ArrayList<>();
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.remove(arr.size() - 1);

        Solution1081 ss = new Solution1081();
        ss.test();
    }

    public String smallestSubsequence(String s) {
        int n = s.length();

        char[] chars = s.toCharArray();
        //顺序覆盖，记录元素最后出现的索引位置
        int[] lastIndex = new int[26];
        for (int i = 0; i < chars.length; i++) {
            lastIndex[chars[i] - 'a'] = i;
        }
        Deque<Character> stack = new ArrayDeque<>();

        //记录下访问过没有
        boolean[] visited = new boolean[26];
        for (int i = 0; i < n; i++) {

            //如果之前访问过了，后边再次看到，直接跳过了，because，前边已经放到了正确的位置上===单调位置
            if (visited[chars[i] - 'a']) {
                continue;
            }

            //栈里边有元素 && 栈顶元素大于遍历到的元素 && 栈顶元素后续还能看到 ===》 弹栈，当前小元素入栈
            //stack.peekLast() > chars[i]：栈顶大，遇到的小，
            //lastIndex[stack.peekLast() - 'a']>i 后边还能遇到
            //把这个之前填写进去的，弹出来，用后边的就可以了，并且标记为没有使用过s
            if (!stack.isEmpty() && stack.peekLast() > chars[i] && lastIndex[stack.peekLast() - 'a'] > i) {
                Character top = stack.pollLast();
                visited[top - 'a'] = false;
            }

            //一般朴素情况，便利到的元素添加到栈内
            stack.addLast(chars[i]);
            visited[chars[i] - 'a'] = true;
        }

        StringBuilder sb = new StringBuilder();
        for (char c : stack) {
            sb.append(c);
        }

        return sb.toString();


    }
}
