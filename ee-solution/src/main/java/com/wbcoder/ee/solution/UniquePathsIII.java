package com.wbcoder.ee.solution;

public class UniquePathsIII {

    public int uniquePathsIII(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;

        int si = 0, sj = 0, ans = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0) {
                    ans++;
                } else if (grid[i][j] == 1) {
                    ans++;
                    si = i;
                    sj = j;
                }
            }
        }
        return dfs(grid, si, sj, ans);
    }



    public int dfs(int[][] grid, int x, int y, int ans) {
        //basecase:
        if (grid[x][y] == 2) return ans == 0 ? 1 : 0;
        int tmp = grid[x][y];
        grid[x][y] = -1;

        int res = 0;

        int m = grid.length, n = grid[0].length;
        //探索
        //定义方向
        int[] dx = {0, 1, 0, -1};
        int[] dy = {1, 0, -1, 0};


        for (int i = 0; i < 4; i++) {
            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 0 && grid[x][y] == 2) {
                //探索，用完就染色，回来就统计结果
                res += dfs(grid, x + dx[i], y + dy[i], ans - 1);
            }
        }
        //回溯
        grid[x][y] = tmp;
        return res;

    }
}
