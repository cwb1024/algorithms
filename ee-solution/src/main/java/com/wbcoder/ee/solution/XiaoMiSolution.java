package com.wbcoder.ee.solution;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.*;

public class XiaoMiSolution {

    public static void main(String[] args) {
        XiaoMiSolution process = new XiaoMiSolution();
        String s = "DDIII";
        int[] ans = process.findPermutation2(s);
        System.out.println(Arrays.toString(ans));
    }

    // 没有考虑连续的情况
    public int[] findPermutation(String s) {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        int n = s.length() + 1;
        int[] ans = new int[n];
        if (s == null || s.length() ==0 ) return ans;
        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            nums[i] = i + 1;
        }
        stack.addLast(nums[0]);
        char[] chars = s.toCharArray();
        int idx = 1;
        for (char ch : chars) {
            if (ch == 'I') {
                stack.addLast(nums[idx]);
            } else if (ch == 'D') {
                int pop = stack.pollLast();
                stack.addLast(nums[idx]);
                stack.addLast(pop);
            } else {
                return ans;
            }
            idx++;
        }
        int i = 0;
        for (Integer v : stack) {
            ans[i++] = v;
        }
        return ans;
    }

    //正确
    public int[] findPermutation1(String s) {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        int n = s.length() + 1;
        int[] ans = new int[n];
        int idx = 0;

        for (int i = 1; i <= n; i++) {
            stack.addLast(i);
            if (i == n || s.charAt(i - 1) == 'I') {
                while (!stack.isEmpty()) {
                    ans[idx++] = stack.pollLast();
                }
            }
        }
        return ans;
    }

    //没有考虑连续的情况
    public int[] findPermutation2(String s) {
        int n = s.length() + 1;
        int[] perm = new int[n];
        perm[0] = 1;

        // 用于存储剩余未放置的整数
        List<Integer> remaining = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            remaining.add(i);
        }
        for (int i = 0; i < s.length(); i++) {
            perm[i + 1] = remaining.get(0);
            if (s.charAt(i) == 'D') {
                swap(perm , i, i + 1);
            }
            remaining.remove(0);
        }
        return perm;
    }

    public void swap(int[] perm, int i, int j) {
        int temp = perm[i];
        perm[i] = perm[j];
        perm[j] = temp;
    }

}
