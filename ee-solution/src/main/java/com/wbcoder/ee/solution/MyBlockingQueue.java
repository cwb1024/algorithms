package com.wbcoder.ee.solution;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingQueue {

    //private int [] queue;
    private ReentrantLock lock;

    private List<Integer> queue;

    private Condition full = lock.newCondition();
    private Condition empty = lock.newCondition();

    private int left;

    private int right;

    private int len;

    public void MyBlockingQueue(){}

    public void MyBlockingQueue(int len){
        queue = new LinkedList<>();
        this.len = len;
        left = 0;
        right = len-1;
    }



    //FIFO
    public int take(){
        try {
            lock.lock();
            if(right == left){
                try {
                    empty.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            queue.get(right);
        }finally {
            lock.unlock();
        }
        return -1;
    }

    public void put(int x){
        try {
            lock.lock();
            if(right - left == len){
                try {
                    full.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            queue.add(x);
        }finally {
            lock.unlock();
        }

    }
}
