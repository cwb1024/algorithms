package com.wbcoder.ee.solution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 有一个表，存了用户每日登陆记录(一天一个)， 写一段程序统计出最大连续登录天数top5 的 （uid, 最大连续登录天数）；
 */
public class XiyinAlgo {

    public List<Integer> getTop5(List<Ulog> ulogs){

        //分组+连续(有效)->结果
        //数据库查询按照时间排序查出来，all
        Map<Integer,Ulog> ulogMaps = new HashMap<>();

        Map<Integer,Integer> ulogMap = new HashMap<>();
        for(Ulog u : ulogs){
            if(ulogMap.get(u.uid) != null){
                if(isVaild(ulogMaps.get(u.uid).loginDay)){
                    ulogMap.put(u.uid,ulogMap.get(u.uid)+1);
                }
            }
            else {
                ulogMap.put(u.uid,1);
            }
        }



        return null;

    }

    public boolean isVaild(String dateStr){

        return false;
    }
}

class Ulog{
    Integer uid;
    String loginDay;
}
